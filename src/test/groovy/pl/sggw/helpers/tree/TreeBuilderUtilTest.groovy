package pl.sggw.helpers.tree

import pl.sggw.helpers.splitters.NominalAttributeSplitter
import pl.sggw.helpers.splitters.Splitter
import pl.sggw.utils.InputDataHelper
import spock.lang.Specification
import spock.lang.Unroll
import spock.util.mop.ConfineMetaClassChanges

class TreeBuilderUtilTest extends Specification {

    def cleanup() {
        TreeBuilderUtil.nominalAttributeSplitter = new NominalAttributeSplitter()
    }

    @ConfineMetaClassChanges([TreeBuilderUtil])
    @Unroll
    def "test find maximum split gain when splitter return empty list"() {
        given:
        Map columns = InputDataHelper.instance.testColumnsContinuous as Map
        Map buildSettings = [targetColumn: 'target', continuousColumns: []]
        Splitter nominalAttributeSplitter = Mock(NominalAttributeSplitter)

        nominalAttributeSplitter.getColumnsPropositionsIndexes(_, _) >> []
        TreeBuilderUtil.nominalAttributeSplitter = nominalAttributeSplitter

        when:
        def maxGain = TreeBuilderUtil.findMaximumSplitGain(columns, buildSettings)

        then:
        maxGain == null
    }
}
