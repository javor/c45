package pl.sggw.helpers.tree

import pl.sggw.utils.InputDataHelper
import spock.lang.Specification


class TreeBuilderTest extends Specification {

    def "test build tree with only nominal values"() {
        given:
        def buildSettings = [
                targetColumn       : 'target',
                targetColumnClasses: ['yes', 'no'],
                positiveTargetClass: 'yes',
                negativeTargetClass: 'no',
                minLeafSize        : 0,
                continuousColumns  : [],
        ]

        def columns = InputDataHelper.instance.getTestColumns() as Map

        when:
        def tree = TreeBuilder.buildTree(columns, buildSettings)

        then:
        def leaf1 = tree[0] as Map
        leaf1.splitInfo.splitAttribute == 'attribute2'
        leaf1.splitInfo.splitRuleText == '"attribute2"=="down"'
        leaf1.predictedTargetClass == 'yes'
        leaf1.positiveTargetCount == 2
        leaf1.negativeTargetCount == 1
        leaf1.size == leaf1.positiveTargetCount + leaf1.negativeTargetCount

        def leaf2 = tree[1] as Map
        leaf2.splitInfo.splitAttribute == 'attribute2'
        leaf2.splitInfo.splitRuleText == '"attribute2"!="down"'
        leaf2.predictedTargetClass == 'no'
        leaf2.positiveTargetCount == 0
        leaf2.negativeTargetCount == 1
        leaf2.size == leaf2.positiveTargetCount + leaf2.negativeTargetCount
    }
}
