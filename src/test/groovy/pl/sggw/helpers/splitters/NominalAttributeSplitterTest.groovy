package pl.sggw.helpers.splitters

import pl.sggw.utils.InputDataHelper
import spock.lang.Specification
import spock.lang.Stepwise

@Stepwise
class NominalAttributeSplitterTest extends Specification {

    def "test nominal attribute split information"() {
        given:
        def columns = InputDataHelper.instance.testColumns as Map
        def attributeColumn = 'attribute1'
        def splitter = new NominalAttributeSplitter()

        when:
        def information = splitter.getColumnsPropositionsIndexes(columns, attributeColumn)
        def info1 = information[0][0]
        def info2 = information[0][1]

        then:
        info1.indexes == [0, 1]
        info1.splitInfo.splitAttribute == attributeColumn
        info1.splitInfo.splitRuleText == "\"${attributeColumn}\"==\"left\""
        (info1.splitInfo.splitRule as Closure)('left')

        info2.indexes == [2, 3]
        info2.splitInfo.splitAttribute == attributeColumn
        info2.splitInfo.splitRuleText == "\"${attributeColumn}\"!=\"left\""
        (info2.splitInfo.splitRule as Closure)('right')
    }

    def "test nominal splitter"() {
        given:
        def columns = InputDataHelper.instance.testColumns as Map
        def attributeColumn = 'attribute1'
        def splitter = new NominalAttributeSplitter()

        when:
        def split = splitter.getColumnsPropositions(columns, attributeColumn)
        def split1 = split[0][0]
        def split2 = split[0][1]

        then:
        split1.splitColumns == (InputDataHelper.instance.testColumnsPartitionsForAttribute1 as List<Map>)[0]
        split1.splitInfo.splitAttribute == attributeColumn
        split1.splitInfo.splitRuleText == "\"${attributeColumn}\"==\"left\""
        (split1.splitInfo.splitRule as Closure)('left')
        !(split1.splitInfo.splitRule as Closure)('right')

        split2.splitColumns == (InputDataHelper.instance.testColumnsPartitionsForAttribute1 as List<Map>)[1]
        split2.splitInfo.splitAttribute == attributeColumn
        split2.splitInfo.splitRuleText == "\"${attributeColumn}\"!=\"left\""
        (split2.splitInfo.splitRule as Closure)('right')
        !(split2.splitInfo.splitRule as Closure)('left')
    }

    def "test nominal splitter return empty list when getColumnsPropositionsIndexes return empty list"() {
        given:
        def splitter = new NominalAttributeSplitter()

        when:
        def split = splitter.getColumnsPropositions([:], null)

        then:
        split == []
    }
}
