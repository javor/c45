package pl.sggw.helpers.splitters

import pl.sggw.utils.InputDataHelper
import spock.lang.Specification
import spock.lang.Stepwise

@Stepwise
class ContinuousAttributeSplitterTest extends Specification {

    def "test continuous attribute split information"() {
        given:
        def columns = InputDataHelper.instance.testColumnsContinuous as Map
        def attributeColumn = 'attribute1'
        def splitter = new ContinuousAttributeSplitter()

        when:
        def information = splitter.getColumnsPropositionsIndexes(columns, attributeColumn)

        then:
        information.size() == (columns[attributeColumn] as List).size() - 1
        def info1 = information[0][0]
        def info2 = information[0][1]

        info1.indexes == [0]
        info1.splitInfo.splitAttribute == attributeColumn
        info1.splitInfo.splitRuleText == "\"${attributeColumn}\"<=\"0\""
        (info1.splitInfo.splitRule as Closure)(0)
        !(info1.splitInfo.splitRule as Closure)(1)
        !(info1.splitInfo.splitRule as Closure)(2)
        !(info1.splitInfo.splitRule as Closure)(3)

        info2.indexes == [1, 2, 3]
        info2.splitInfo.splitAttribute == attributeColumn
        info2.splitInfo.splitRuleText == "\"${attributeColumn}\">\"0\""
        !(info2.splitInfo.splitRule as Closure)(0)
        (info2.splitInfo.splitRule as Closure)(1)
        (info2.splitInfo.splitRule as Closure)(2)
        (info2.splitInfo.splitRule as Closure)(3)
    }

    def "test continuous splitter"() {
        given:
        def columns = InputDataHelper.instance.testColumnsContinuous as Map
        def attributeColumn = 'attribute1'
        def splitter = new ContinuousAttributeSplitter()

        when:
        def split = splitter.getColumnsPropositions(columns, attributeColumn)
        def split1 = split[0][0]
        def split2 = split[0][1]

        then:
        split.size() == (columns[attributeColumn] as List).size() - 1

        split1.splitColumns == (InputDataHelper.instance.testColumnsContinuousPartitionsForAttribute1 as List<Map>)[0]
        split1.splitInfo.splitAttribute == attributeColumn
        split1.splitInfo.splitRuleText == "\"${attributeColumn}\"<=\"0\""
        (split1.splitInfo.splitRule as Closure)(0)
        !(split1.splitInfo.splitRule as Closure)(1)
        !(split1.splitInfo.splitRule as Closure)(2)
        !(split1.splitInfo.splitRule as Closure)(3)

        split2.splitColumns == (InputDataHelper.instance.testColumnsContinuousPartitionsForAttribute1 as List<Map>)[1]
        split2.splitInfo.splitAttribute == attributeColumn
        split2.splitInfo.splitRuleText == "\"${attributeColumn}\">\"0\""
        !(split2.splitInfo.splitRule as Closure)(0)
        (split2.splitInfo.splitRule as Closure)(1)
        (split2.splitInfo.splitRule as Closure)(2)
        (split2.splitInfo.splitRule as Closure)(3)
    }

    def "test continuous splitter return empty list when getColumnsPropositionsIndexes return empty list"() {
        given:
        def splitter = new ContinuousAttributeSplitter()

        when:
        def split = splitter.getColumnsPropositions([:], null)

        then:
        split == []
    }
}
