package pl.sggw.algorithm

import pl.sggw.utils.InputDataHelper
import spock.lang.Specification
import spock.lang.Stepwise

@Stepwise
class C45Spec extends Specification {

    def "test entropy information without null values"() {
        given:
        def columns = InputDataHelper.instance.testColumns as Map
        def targetColumn = 'target'

        when:
        def entropyInformation = C45.entropyInformation(columns, targetColumn)

        then:
        entropyInformation.round(5) == 1.0
    }

    def "test entropy"() {
        given:
        def partitions = InputDataHelper.instance.testColumnsPartitionsForAttribute1 as List<Map>
        def targetColumn = 'target'
        def tableCount = partitions.sum { (it[targetColumn] as List).size() } as int

        when:
        def entropy = C45.entropy(tableCount, partitions, targetColumn)

        then:
        entropy == 1
    }

    def "test gain"() {
        given:
        def columns = InputDataHelper.instance.testColumns as Map
        def partitions = InputDataHelper.instance.testColumnsPartitionsForAttribute1 as List<Map>
        def targetColumn = 'target'

        when:
        def gain = C45.gain(columns, partitions, targetColumn)

        then:
        gain == 0
    }
}
