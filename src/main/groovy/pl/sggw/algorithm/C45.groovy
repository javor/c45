package pl.sggw.algorithm

import static groovyx.gpars.GParsPool.withPool

class C45 {

    static entropyInformation(Map columns, String targetColumn) {
        def targetColumnRows = columns[targetColumn] as List
        def targetColumnRowsCount = targetColumnRows.size()
        def targetColumnClasses = (targetColumnRows as Set) as List

        def sum = withPool {
            targetColumnClasses.collectParallel {
                def p = targetColumnRows.count(it) / targetColumnRowsCount
                p * (Math.log(p) / Math.log(2))
            }.sumParallel()
        }

        -(sum ?: 0)
    }

    static entropy(int tableCount, List<Map> partitions, String targetColumn) {
        def sum = withPool {
            partitions.collectParallel {
                def partitionEntropyInformation = entropyInformation(it as Map, targetColumn) as double
                ((it[targetColumn] as List).size() / tableCount) * partitionEntropyInformation
            }.sumParallel()
        }

        sum ?: 0
    }

    /**
     * Gain(A) = Info(D) - InfoA(D)
     */
    static gain(Map<String, List> columns, List<Map> columnsPartitions, String targetColumn) {
        def tableCount = columns[targetColumn].size()
        return entropyInformation(columns, targetColumn) - entropy(tableCount, columnsPartitions, targetColumn)
    }
}
