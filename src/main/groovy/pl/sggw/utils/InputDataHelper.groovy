package pl.sggw.utils

import groovy.json.JsonSlurper

@Singleton
class InputDataHelper {

    private static final JsonSlurper jsonSlurper = new JsonSlurper()

    static getColumnsFromFile(File file) {
        jsonSlurper.parseText(file.getText('UTF-8')) as Map
    }

    def getTestColumns() {
        def resource = getClass().getClassLoader().getResource('inputFile.json')
        jsonSlurper.parseText(new File(resource.getText('UTF-8')) as String)
    }

    def getTestColumnsPartitionsForAttribute1() {
        def resource = getClass().getClassLoader().getResource('partitionsAttribute1.json')
        jsonSlurper.parseText(new File(resource.getText('UTF-8')) as String)
    }

    def getTestColumnsContinuous() {
        def resource = getClass().getClassLoader().getResource('inputFileContinuous.json')
        jsonSlurper.parseText(new File(resource.getText('UTF-8')) as String)
    }

    def getTestColumnsContinuousPartitionsForAttribute1() {
        def resource = getClass().getClassLoader().getResource('partitionsContinuousAttribute1.json')
        jsonSlurper.parseText(new File(resource.getText('UTF-8')) as String)
    }
}
