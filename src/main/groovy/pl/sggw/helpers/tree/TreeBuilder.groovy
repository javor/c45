package pl.sggw.helpers.tree

class TreeBuilder {

    static buildTree(Map learnColumns, Map buildSettings, level = 5) {

        def tree = []

        def splitsPropositions = TreeBuilderUtil.findMaximumSplitGain(learnColumns, buildSettings)?.splits ?: []

        splitsPropositions.each { splitProposition ->
            def leafLearnColumns = splitProposition.splitColumns as Map
            def leafLearnColumnsValues = leafLearnColumns[buildSettings.targetColumn as String] as List

            def predictedTargetClass = (leafLearnColumnsValues.size() / 2) < leafLearnColumnsValues.count(
                    buildSettings.positiveTargetClass) ?
                    buildSettings.positiveTargetClass : buildSettings.negativeTargetClass

            def leaf = [
                    leaves              : [],
                    splitInfo           : splitProposition.splitInfo,
                    predictedTargetClass: predictedTargetClass,
                    positiveTargetCount : leafLearnColumnsValues.count(buildSettings.positiveTargetClass),
                    negativeTargetCount : leafLearnColumnsValues.count(buildSettings.negativeTargetClass),
                    size                : leafLearnColumnsValues.size(),
            ]

            tree.add(leaf)

            if (level == 0 || buildSettings.minLeafSize >= leaf.size || (leafLearnColumnsValues as Set).size() <= 1) {
                return
            }

            leaf.leaves.addAll(buildTree(leafLearnColumns, buildSettings, level - 1))
        }

        tree
    }
}
