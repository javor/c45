package pl.sggw.helpers.tree

class TreeVerifier {

    static verifyTree(Map columns, Map buildSettings, List tree) {
        def costMatrix = [TP: 0, FN: 0, FP: 0, TN: 0, NC: 0]

        (columns[buildSettings.targetColumn] as List).withIndex().each { columnValues, index ->
            try {
                def testedValue = testValue(columns, buildSettings, tree, index as int) as Map
                costMatrix.TP += testedValue.TP
                costMatrix.FN += testedValue.FN
                costMatrix.FP += testedValue.FP
                costMatrix.TN += testedValue.TN
            } catch (IllegalStateException ignored) {
                costMatrix.NC += 1
            }
        }

        costMatrix
    }

    static testValue(Map columns, Map buildSettings, List tree, int index) {

        def found = tree.find { leaf ->
            def testRule = ((leaf as Map).splitInfo.splitRule as Closure)
            testRule((columns[(leaf as Map).splitInfo.splitAttribute] as List)[index])
        } as Map

        if (found == null) {
            throw new IllegalStateException('Not matching leaf')
        }

        if (found && found.leaves.size > 0) {
            try {
                return testValue(columns, buildSettings, found.leaves as List, index)
            } catch (IllegalStateException ignored) {
            }
        }

        def cls = (columns[buildSettings.targetColumn] as List)[index]
        [
                TP: (buildSettings.positiveTargetClass == cls) && (cls == found?.predictedTargetClass) ? 1 : 0,
                FN: (buildSettings.positiveTargetClass == cls) && (cls != found?.predictedTargetClass) ? 1 : 0,
                FP: (buildSettings.negativeTargetClass == cls) && (cls != found?.predictedTargetClass) ? 1 : 0,
                TN: (buildSettings.negativeTargetClass == cls) && (cls == found?.predictedTargetClass) ? 1 : 0
        ]
    }
}
