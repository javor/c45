package pl.sggw.helpers.tree

class TreeUtil {

    static printTree(List tree, long level = 0) {
        tree.each { leaf ->
            print("${'\t' * level}|\n${'\t' * level} \\ ${(leaf as Map).splitInfo.splitRuleText}\n")
            printTree((leaf as Map).leaves as List, level + 1)
        }
    }
}
