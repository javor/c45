package pl.sggw.helpers.tree

import pl.sggw.algorithm.C45
import pl.sggw.helpers.splitters.ContinuousAttributeSplitter
import pl.sggw.helpers.splitters.NominalAttributeSplitter
import pl.sggw.helpers.splitters.Splitter

class TreeBuilderUtil {

    private static Splitter continuousAttributeSplitter = new ContinuousAttributeSplitter()

    private static Splitter nominalAttributeSplitter = new NominalAttributeSplitter()

    static Map findMaximumSplitGain(Map columns, Map buildSettings) {
        columns.findAll { k, v -> k != buildSettings.targetColumn }.collect { k, v ->
            ((k in buildSettings.continuousColumns) ? continuousAttributeSplitter : nominalAttributeSplitter)
                    .getColumnsPropositions(columns, k as String).findAll { it -> !it.isEmpty() }.collect() {
                [gain  : C45.gain(columns, it.collect { e -> e.splitColumns }, buildSettings.targetColumn as String),
                 splits: it]
            }?.max { it?.gain }
        }?.max { it?.gain }
    }
}
