package pl.sggw.helpers.splitters

abstract class Splitter {

    abstract List<List<List>> getColumnsPropositionsIndexes(Map columns, String splitAttribute)

    List<List<Map>> getColumnsPropositions(Map columns, String splitAttribute) {

        getColumnsPropositionsIndexes(columns, splitAttribute).collect {
            it.collect { splitProposition ->
                [
                        splitColumns: columns.collectEntries { columnName, columnsValues ->
                            [
                                    (columnName): (columnsValues as List).withIndex().split { _, elementIndex ->
                                        (splitProposition as Map).indexes.contains(elementIndex as long)
                                    }[0].collect { it[0] }
                            ]
                        },
                        splitInfo   : (splitProposition as Map).splitInfo
                ]
            }
        }
    }

    static Map stratification(Map columns, Map buildSettings) {

        def stratificationPercent = buildSettings.stratificationPercent as long
        stratificationPercent < 0 ? 0 : stratificationPercent
        stratificationPercent > 100 ? 100 : stratificationPercent

        def positive = (columns[buildSettings.targetColumn] as List).count(buildSettings.positiveTargetClass)
        def negative = (columns[buildSettings.targetColumn] as List).count(buildSettings.negativeTargetClass)

        int positiveRatio = Math.round(stratificationPercent / 100 * positive + 0.5)
        int negativeRatio = Math.round(stratificationPercent / 100 * negative + 0.5)

        def positiveIndexes = (columns[buildSettings.targetColumn] as List).findIndexValues {
            it == buildSettings.positiveTargetClass
        }

        def negativeIndexes = (columns[buildSettings.targetColumn] as List).findIndexValues {
            it == buildSettings.negativeTargetClass
        }

        Collections.shuffle(positiveIndexes)
        def randomPositive = positiveIndexes.take(positiveRatio)

        Collections.shuffle(negativeIndexes)
        def randomNegative = negativeIndexes.take(negativeRatio)

        def learnColumns = [:]
        def testColumns = [:]

        columns.each { columnName, columnValue ->

            def split = (columnValue as List).withIndex().split { it, index ->
                (index as long) in randomPositive || (index as long) in randomNegative
            }

            learnColumns[columnName] = split[0].collect { it[0] }
            testColumns[columnName] = split[1].collect { it[0] }

        }

        [learnColumns: learnColumns, testColumns: testColumns]
    }
}
