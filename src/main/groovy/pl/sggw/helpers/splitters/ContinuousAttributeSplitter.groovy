package pl.sggw.helpers.splitters

class ContinuousAttributeSplitter extends Splitter {

    @Override
    List<List<Map>> getColumnsPropositionsIndexes(Map columns, String splitAttribute) {

        try {
            ((columns[splitAttribute] as Set) as List).collect { attributeClass ->
                [
                        [
                                indexes  : columns[splitAttribute].findIndexValues { value -> attributeClass >= value },
                                splitInfo: [
                                        splitAttribute: splitAttribute,
                                        splitRule     : { attributeClass >= it },
                                        splitRuleText : "\"${splitAttribute}\"<=\"${attributeClass}\""
                                ]
                        ],
                        [
                                indexes  : columns[splitAttribute].findIndexValues { value -> attributeClass < value },
                                splitInfo: [
                                        splitAttribute: splitAttribute,
                                        splitRule     : { attributeClass < it },
                                        splitRuleText : "\"${splitAttribute}\">\"${attributeClass}\""
                                ]
                        ]
                ]
            }[0..<-1]
        } catch (IndexOutOfBoundsException ignored) {
            []
        }
    }
}
