package pl.sggw

import groovy.time.TimeCategory
import groovyjarjarcommonscli.Option
import pl.sggw.helpers.splitters.Splitter
import pl.sggw.helpers.tree.TreeBuilder
import pl.sggw.helpers.tree.TreeUtil
import pl.sggw.helpers.tree.TreeVerifier
import pl.sggw.utils.InputDataHelper

class DecisionTree {

    static void main(String[] args) {

        def cli = new CliBuilder(usage: 'classify')
        cli.if(longOpt: 'inputFile', 'Input file for learn phase', args: 1, required: true)
        cli.t(longOpt: 'target', 'Target column name', args: 1, required: true)
        cli.tv(longOpt: 'positiveTargetValue', 'Target positive value', args: 1, required: true)

        cli.sp(longOpt: 'stratificationPercent', 'Stratification percent (How many elements for learn phase', args: 1)

        cli.mls(longOpt: 'minLeafSize', 'Minimal leaf size', args: 1)
        cli.mlsp(longOpt: 'minLeafSizePercent', 'Minimal leaf size percent', args: 1)

        cli.d(longOpt: 'depth', 'Max tree depth', args: 1)
        cli.cc(longOpt: 'continuousColumns', 'Continuous columns (separated by commas)',
                args: Option.UNLIMITED_VALUES, valueSeparator: ',')

        def options = cli.parse(args)

        def inputFile = options.if as String
        def columns = InputDataHelper.instance.getColumnsFromFile(new File(inputFile)) as Map

        def targetColumnName = options.t
        def positiveTargetValue = options.tv

        def buildSettings = [
                targetColumn         : targetColumnName,
                continuousColumns    : options.ccs ?: [],
                targetColumnClasses  : (columns[targetColumnName] as Set) as List,
                stratificationPercent: options.sp ?: 70
        ]

        buildSettings.positiveTargetClass = positiveTargetValue
        buildSettings.negativeTargetClass =
                "${(buildSettings.targetColumnClasses as List).find { it != positiveTargetValue }}"


        def stratification = Splitter.stratification(columns, buildSettings)

        def learnColumns = stratification.learnColumns
        def testColumns = stratification.testColumns

        def minLeafSize = ((10 / 100) as double) * ((learnColumns[targetColumnName] as List).size() as double) as long

        if (options.mls) {
            def mls = options.mls as long
            mls = mls < 0 ? 0 : mls
            mls = mls > (learnColumns[options.t].size() as long) ? (learnColumns[targetColumnName] as List).size() : mls
            minLeafSize = mls
        }

        if (options.mlsp) {
            def mlsp = options.mlsp as long
            mlsp = mlsp < 0 ? 0 : mlsp
            mlsp = mlsp > 100 ? 100 : mlsp
            minLeafSize = ((mlsp as long) / 100) * (learnColumns[targetColumnName] as List).size() as long
        }

        buildSettings.minLeafSize = minLeafSize

        println('Build settings:'.center(79, '-') + '\n')
        println("\ttargetColumn: \"${buildSettings.targetColumn}\"")
        println("\tpositiveTargetClass: \"${buildSettings.positiveTargetClass}\"")
        println("\tnegativeTargetClass: \"${buildSettings.negativeTargetClass}\"")
        println("\tminLeafSize: ${buildSettings.minLeafSize}")
        println("\tcontinuousColumns: ${buildSettings.continuousColumns.collect { "\"${it}\"" }}")

        print('\n')

        println('Files'.center(79, '-') + '\n')
        println("\t${inputFile}")

        print('\n')

        def startDate = new Date()

        def tree = TreeBuilder.buildTree(learnColumns, buildSettings, (options.d ?: 5) as long)

        print('\n')
        println('Tree'.center(79, '-') + '\n')
        TreeUtil.printTree(tree)

        def costMatrix = TreeVerifier.verifyTree(testColumns, buildSettings, tree)

        def endDate = new Date()

        print('\n')
        println('Cost Matrix'.center(79, '-') + '\n')
        println("\tTP: ${costMatrix.TP}\n\tFN: ${costMatrix.FN}\n\tFP: ${costMatrix.FP}\n\tTN: ${costMatrix.TN}")
        println("\tNC: ${costMatrix.NC}")

        print('\n')
        println('Statistics'.center(79, '-') + '\n')
        def allColumnsCount = columns[targetColumnName].size
        println("\tAll: ${allColumnsCount}")
        def learnColumnsCount = learnColumns[targetColumnName].size
        def testColumnsCount = testColumns[targetColumnName].size
        println("\tL count: ${learnColumnsCount}\tT count: ${testColumnsCount}")
        println("\tNot used: ${allColumnsCount - learnColumnsCount - testColumnsCount}")
        println("\tsensitivity: ${costMatrix.TP / ((costMatrix.TP + costMatrix.FN) == 0 ? 1 : (costMatrix.TP + costMatrix.FN))}")
        println("\tspecifity: ${costMatrix.TN / ((costMatrix.FP + costMatrix.TN) == 0 ? 1 : (costMatrix.FP + costMatrix.TN))}")

        print('\n')
        println('Time'.center(79, '-') + '\n')
        use(TimeCategory) {
            println("${endDate - startDate}".center(79))
        }
    }
}
